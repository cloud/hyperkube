#!/bin/bash
set -x
set -e
raw_git_server="https://gitlab.cern.ch/cloud-infrastructure/kubernetes/-/raw"
cern_repo_tags="$(skopeo inspect docker://${CI_REGISTRY_IMAGE}:v1.19.0-cern.0 | jq .RepoTags -c | sed -E 's/\[|\]|\/|\"|,/ /g')"
echo $cern_repo_tags
bump_versions=$(cat bump-versions | xargs)

function build_image {
    version=$1
    sha512=$2

    for build_release in $(seq 0 100) ; do
        tag="${version}-cern.${build_release}"
        # We don't duplicate a build.
        if [[ ! ${cern_repo_tags[*]} =~ ${tag} ]] ; then
            echo "INFO Try to build: $CI_REGISTRY_IMAGE:${tag} ${sha512}"
            echo "docker build -f Dockerfile.amd64 --build-arg KUBE_VERSION=${version} --build-arg SHA512=${sha512} -t $CI_REGISTRY_IMAGE:${tag} ."
            echo "docker push ${CI_REGISTRY_IMAGE}:${tag}"
            if [ "${PUSH_IMAGES}" = "true" ] ; then
                docker build -f Dockerfile.amd64 --build-arg KUBE_VERSION=${version} --build-arg SHA512=${sha512} -t "${CI_REGISTRY_IMAGE}:${tag}" .
                docker push "${CI_REGISTRY_IMAGE}:${tag}"
                docker system prune -a -f
            else
                echo "INFO Skipping build."
            fi
            return 0
        # We want to add an extra build for this version.
        elif [[ ${bump_versions[*]} =~ ${tag} ]] ; then
            echo "INFO $CI_REGISTRY_IMAGE:${tag} FOUND. Bump version"
            # continue
        else
            echo "INFO $CI_REGISTRY_IMAGE:${tag} FOUND. Skip build."
            return 0
        fi
        i=$((i++))
    done
}

# Get all changelogs
changelogs=$(curl -s ${raw_git_server}/master/CHANGELOG/README.md | grep -E -o "./CHANGELOG.*.md" | sed -E 's#./|CHANGELOG-|.md##g' | xargs)
# For every minor release
for changelog in $(echo $changelogs)
do
    # Only for releases greater or equal to 1.19
    max_version=$(printf "1.19\n${changelog}" | sort -V -r | head -1)
    if [ "${max_version}" = "${changelog}" ] ; then
        curl -s -o changelog.md ${raw_git_server}/master/CHANGELOG/CHANGELOG-${changelog}.md
        versions=$(cat changelog.md | grep -E -o "https://dl.k8s.io/.*/kubernetes-server-linux-amd64.tar.gz" | sed -E 's#https://dl.k8s.io/##g' | sed -E 's#/kubernetes-server-linux-amd64.tar.gz##g')
        # for every point release
        for version in $(echo ${versions} | xargs)
        do
            sha512=$(cat changelog.md | grep "/${version}/" | grep kubernetes-server-linux-amd64.tar.gz  | grep -o -E "https://dl.k8s.io.*" | sed -E 's/`|\)|\|//g' | awk '{print $2}')
            build_image $version $sha512
        done
    fi
done
